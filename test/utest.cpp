#include "gtest/gtest.h"
#include <tic_tac_toe.h>
#include <game_utils.h>
#include <human.h>
#include <alpha_beta_ai.h>

// tests that moves are initialized correctly
TEST(TicTacToe, testInitialMoves)
{
    TicTacToe game;
    State test_state;
    for(int i = 1; i<=game.n_; i++)
    {
        for(int j = 1; j<=game.n_; j++)
        {
            test_state.moves.insert(Move(i,j));
            test_state.board[Move(i,j)] = '_';
        }
    }
    EXPECT_EQ(game.state.moves, test_state.moves);
}

// tests the result function
TEST(TicTacToe, testResult)
{
    TicTacToe game;
    State expected_state;
    auto state = game.result(Move(1,1), game.state);
    for(int i = 1; i<=game.n_; i++)
    {
        for(int j = 1; j<=game.n_; j++)
        {
            expected_state.moves.insert(Move(i,j));
            expected_state.board[Move(i,j)] = '_';
        }
    }
    expected_state.board[Move(1,1)] = 'X';
    expected_state.moves.erase(Move(1,1));
    expected_state.to_move = 'O';
    expected_state.utility = 0;

    for(auto pair : expected_state.board){
        EXPECT_EQ(expected_state.board[pair.first], state.board[pair.first]);
    }
    EXPECT_EQ(expected_state.moves, state.moves);
    EXPECT_EQ(expected_state.to_move, state.to_move);
}

// tests that the compute utility function works for x in row 1
TEST(TicTacToe, testComputeUtilityX_Row1)
{
    auto game = TicTacToe();
    game.state.board[Move(1,1)] = 'X';
    game.state.board[Move(1,2)] = 'X';
    auto utility = game.compute_utility(Move(1,3), game.state);
    EXPECT_EQ(1, utility);
}

// tests that the compute utility function works for x in row 2
TEST(TicTacToe, testComputeUtilityX_Row2)
{
    auto game = TicTacToe();
    game.state.board[Move(2,1)] = 'X';
    game.state.board[Move(2,2)] = 'X';
    auto utility = game.compute_utility(Move(2,3), game.state);
    EXPECT_EQ(1, utility);
}

// tests that the compute utility function works for x in row 3
TEST(TicTacToe, testComputeUtilityX_Row3)
{
    auto game = TicTacToe();
    game.state.board[Move(3,1)] = 'X';
    game.state.board[Move(3,2)] = 'X';
    auto utility = game.compute_utility(Move(3,3), game.state);
    EXPECT_EQ(1, utility);
}

// tests that the compute utility function works for x in col 1
TEST(TicTacToe, testComputeUtilityX_Col1)
{
    auto game = TicTacToe();
    game.state.board[Move(1,1)] = 'X';
    game.state.board[Move(2,1)] = 'X';
    auto utility = game.compute_utility(Move(3,1), game.state);
    EXPECT_EQ(1, utility);
}

// tests that the compute utility function works for x in col 2
TEST(TicTacToe, testComputeUtilityX_Col2)
{
    auto game = TicTacToe();
    game.state.board[Move(1,2)] = 'X';
    game.state.board[Move(2,2)] = 'X';
    auto utility = game.compute_utility(Move(3,2), game.state);
    EXPECT_EQ(1, utility);
}

// tests that the compute utility function works for x in col 3
TEST(TicTacToe, testComputeUtilityX_Col3)
{
    auto game = TicTacToe();
    game.state.board[Move(1,3)] = 'X';
    game.state.board[Move(2,3)] = 'X';
    auto utility = game.compute_utility(Move(3,3), game.state);
    EXPECT_EQ(1, utility);
}

// tests that the compute utility function works for x in the 
// upper left -> lower right diagonal
TEST(TicTacToe, testComputeUtilityX_LRDiag)
{
    auto game = TicTacToe();
    game.state.board[Move(1,1)] = 'X';
    game.state.board[Move(2,2)] = 'X';
    auto utility = game.compute_utility(Move(3,3), game.state);
    EXPECT_EQ(1, utility);
}
 // tests that the compute utility function works for x
 // in the upper right -> lower left diagonal
TEST(TicTacToe, testComputeUtilityX_RLDiag)
{
    auto game = TicTacToe();
    game.state.board[Move(3,1)] = 'X';
    game.state.board[Move(2,2)] = 'X';
    auto utility = game.compute_utility(Move(1,3), game.state);
    EXPECT_EQ(1, utility);
}

// tests that the compute utility function works for O in row 1
TEST(TicTacToe, testComputeUtilityO_Row1)
{
    auto game = TicTacToe();
    game.state.board[Move(1,1)] = 'O';
    game.state.board[Move(1,2)] = 'O';
    game.state.to_move = 'O';
    auto utility = game.compute_utility(Move(1,3), game.state);
    EXPECT_EQ(-1, utility);
}

// tests that the compute utility function works for O in row 2
TEST(TicTacToe, testComputeUtilityO_Row2)
{
    auto game = TicTacToe();
    game.state.board[Move(2,1)] = 'O';
    game.state.board[Move(2,2)] = 'O';
    game.state.to_move = 'O';
    auto utility = game.compute_utility(Move(2,3), game.state);
    EXPECT_EQ(-1, utility);
}

// tests that the compute utility function works for O in row 3
TEST(TicTacToe, testComputeUtilityO_Row3)
{
    auto game = TicTacToe();
    game.state.board[Move(3,1)] = 'O';
    game.state.board[Move(3,2)] = 'O';
    game.state.to_move = 'O';
    auto utility = game.compute_utility(Move(3,3), game.state);
    EXPECT_EQ(-1, utility);
}

// tests that the compute utility function works for O in col 1
TEST(TicTacToe, testComputeUtilityO_Col1)
{
    auto game = TicTacToe();
    game.state.board[Move(1,1)] = 'O';
    game.state.board[Move(2,1)] = 'O';
    game.state.to_move = 'O';
    auto utility = game.compute_utility(Move(3,1), game.state);
    EXPECT_EQ(-1, utility);
}

// tests that the compute utility function works for O in col 2
TEST(TicTacToe, testComputeUtilityO_Col2)
{
    auto game = TicTacToe();
    game.state.board[Move(1,2)] = 'O';
    game.state.board[Move(2,2)] = 'O';
    game.state.to_move = 'O';
    auto utility = game.compute_utility(Move(3,2), game.state);
    EXPECT_EQ(-1, utility);
}

// tests that the compute utility function works for O in col 3
TEST(TicTacToe, testComputeUtilityO_Col3)
{
    auto game = TicTacToe();
    game.state.board[Move(1,3)] = 'O';
    game.state.board[Move(2,3)] = 'O';
    game.state.to_move = 'O';
    auto utility = game.compute_utility(Move(3,3), game.state);
    EXPECT_EQ(-1, utility);
}

// tests that the compute utility function works for o in the 
// upper left -> lower right diagonal
TEST(TicTacToe, testComputeUtilityO_LRDiag)
{
    auto game = TicTacToe();
    game.state.board[Move(1,1)] = 'O';
    game.state.board[Move(2,2)] = 'O';
    game.state.to_move = 'O';
    auto utility = game.compute_utility(Move(3,3), game.state);
    EXPECT_EQ(-1, utility);
}

// tests that the compute utility function works for o in the 
// upper right -> lower left diagonal
TEST(TicTacToe, testComputeUtilityO_RLDiag)
{
    auto game = TicTacToe();
    game.state.board[Move(3,1)] = 'O';
    game.state.board[Move(2,2)] = 'O';
    game.state.to_move = 'O';
    auto utility = game.compute_utility(Move(1,3), game.state);
    EXPECT_EQ(-1, utility);
}

// tests that game over returns true for an x win
TEST(TicTacToe, testGameOver_Xwin)
{
    TicTacToe game;
    game.state.utility = 1;
    auto check = game.game_over(game.state);
    EXPECT_EQ(check, true);
}

// tests that game over returns true for an o win
TEST(TicTacToe, testGameOver_Owin)
{
    TicTacToe game;
    game.state.utility = -1;
    auto check = game.game_over(game.state);
    EXPECT_EQ(check, true);
}

// tests that game over returns false when game isn't over
TEST(TicTacToe, testGameOver_Nowin)
{
    TicTacToe game;
    game.state.utility = 0;
    auto check = game.game_over(game.state);
    EXPECT_EQ(check, false);
}

// tests that game over returns true when there are no moves
TEST(TicTacToe, testGameOver_NoMoves)
{
    TicTacToe game;
    game.state.moves.clear();
    auto check = game.game_over(game.state);
    EXPECT_EQ(check, true);
}

// tests that alpha beta ai returns a valid move
TEST(AlphaBetaAI, testValidAlphaBetaAIMove)
{
    TicTacToe game;
    AlphaBetaAI test_ai;
    auto move = test_ai.play(game);
    auto check = game.state.moves.find(move) !=game.state.moves.end();
    EXPECT_EQ(check, true);
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}