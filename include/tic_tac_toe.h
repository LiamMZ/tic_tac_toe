#pragma once

#include <game_utils.h>
#include <vector>
#include <string>

class TicTacToe
{
public:
    /**
     * @brief Constructor initializes game board and available moves list
     * 
     * @param[in] n this corresponds to the number of rows and cols
     * in the game board and number of tiles in a row to win
     */ 
    TicTacToe(int n = 3);

    /**
     * @brief Computes the hypothetical resulting state if the given move is made in 
     *          the given state
     * @param[in] move proposed move
     * @param[in] state proposed state in which move will be made
     * @returns state hypothetical resulting state of move in state
     */ 
    State result(Move move, State state);

    /**
     * @brief Computes the utility of the given state
     * 
     * @param[in] move the proposed move to be taken
     * @param[in] state the proposed state in which the move will be made
     * @returns utility the value of the state to the player +1 = X win, -1 = O-win, 0 = non-terminating state
     */
    int compute_utility(Move move, State state);

    /**
     * @brief Checks if the given state is a game terminating state
     * @param[in] state state to check
     * @returns bool 1 if a player wins or there are no more moves, 0 if the game
     *          is not over
     */
    bool game_over(State& state);

    /**
     * @brief Returns the utility of a state to the given player
     * @param[in] state proposed state to check
     * @param[in] player the player to check state utility for
     * @returns utility the utility of the state to the given player +1 = win, -1 = Lose, 0 = non-terminating state
     */
    int utility(State& state, char player);

    /**
     * @brief Outputs the board of the given state to the terminal
     * @param[in] state the state to display board for
     */
    void display(State& state);
    int n_; // the number of rows and cols in the game board and number of tiles in a row to win
    State state; // the current state of a tic-tac-toe game
};