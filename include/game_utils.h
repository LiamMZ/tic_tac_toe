#pragma once

#include <unordered_map>
#include <unordered_set>

struct Move
{   
    Move(){}
    Move(int row_, int col_)
    {
        row = row_;
        col = col_;
    }
    bool operator==(const Move& move) const
    {
        return row==move.row & col==move.col;
    }
    int row;
    int col;
};
class MyHashFunction { 
public: 
    size_t operator()(const Move& move) const
    { 
        int x = ((move.row >> 16) ^ move.row) * 0x45d9f3b;
        x = ((x >> 16) ^ x) * 0x45d9f3b;
        x = (x >> 16) ^ x;
        int y = ((move.col >> 16) ^ move.col) * 0x45d9f3b;
        y = ((y >> 16) ^ y) * 0x45d9f3b;
        y = (y >> 16) ^ y;
        return x^y; 
    } 
};

struct State{
    char to_move = 'X';
    int utility = 0;
    std::unordered_map<Move, char, MyHashFunction> board;
    std::unordered_set<Move, MyHashFunction> moves;
};
