#pragma once

#include <game_utils.h>
#include <tic_tac_toe.h>
#include <sstream>

/**
 * @brief Abstract class for a TicTacToe Player
 */
class Player
{
public:
    /**
     * @brief Pure Virtual function for a player to choose
     *          a move required for all players
     */
    virtual Move play(TicTacToe& game) = 0;
};