#include <tic_tac_toe.h>
#include <player.h>

class Human : Player
{
public:
    virtual Move play(TicTacToe& game);
private:
    /**
     * @brief Function to validate input from a human user
     * @param[in] row user inputted row
     * @param[in] col user inputted column
     * @param[in] game instance of game that is currently being played
     * @param[in] move reference of move to hold move once validated
     * @return bool indicates whether the input is valid
     */
    bool validate_input(std::string row, std::string col, TicTacToe& game, Move& move);

    /**
     * @brief Function to handle user input
     * @param[in] game instance of TicTacToe game currently being played
     * @returns move a valid move based on current game
     */
    Move handle_input(TicTacToe& game);
};