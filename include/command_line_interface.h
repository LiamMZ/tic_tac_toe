#include<interface.h>
#include<string>
#include<tic_tac_toe.h>

/**
 * @brief Class to provide a basic command line interface
 */
class CLInterface : Interface
{
public:
    /**
     * @brief Overwrites the pure virtual run function for interfaces
     */
    virtual void run(int n);
private:
    /**
     * @brief Function for playing a game of tic tac toe
     * @param[in] player1 the first player of the game
     * @param[in] player2 the second player of the game
     * @param[in] game the instanc of the game of TicTacToe currently being played
     */
    template <class T1, class T2>
    std::string play_game(T1 player1, T2 player2, TicTacToe& game);

    /**
     * @brief Function for validating menu input
     * @param[in] input user input for menue
     */
    bool handlMenuInput(std::string input);
};