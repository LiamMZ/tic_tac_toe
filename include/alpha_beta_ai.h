#include <tic_tac_toe.h>
#include <game_utils.h>
#include <player.h> 

class AlphaBetaAI : Player
{
public:
    virtual Move play(TicTacToe& game);
private:
    /**
     * @brief Function that prerforms alpha beta search
     * @param[in] game instance of TicTacToe game currently being played
     * @returns move a valid move based on current game
     */
    Move alphabeta_search(TicTacToe& game);

    /**
     * @brief Function for Maximizing player in minimax algorithm
     * @param[in] game instance of TicTacToe game currently being played
     * @param[in] state current state being considered
     * @param[in] alpha best/highest score found so far for Max
     * @param[in] beta best/lowest score found so far for Min
     * @param[in] value reference to value which holds the highest value found
     */
    Move max_value(TicTacToe& game, State state, int alpha, int beta, int& value);

    /**
     * @brief Function for Minimizing player in minimax algorithm
     * @param[in] game instance of TicTacToe game currently being played
     * @param[in] state current state being considered
     * @param[in] alpha best/highest score found so far for Max
     * @param[in] beta best/lowest score found so far for Min
     * @param[in] value reference to value which holds the highest value found
     */
    Move min_value(TicTacToe& game, State state, int alpha, int beta, int& value);
};