
/**
 * @brief Abstract base class for user interfaces
 */
class Interface
{
    public:
    virtual void run(int n) = 0;
};