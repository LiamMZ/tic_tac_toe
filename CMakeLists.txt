cmake_minimum_required(VERSION 3.1.0)
project(tic_tac_toe)

# set the C++14 standard
set(CMAKE_CXX_STANDARD 14)

find_package(GTest REQUIRED)
find_package(Threads REQUIRED)


include_directories(
  include
)

set(${PROJECT_NAME}_SRC_FILES
    src/alpha_beta_ai.cpp
    src/human.cpp
    src/command_line_interface.cpp
    src/tic_tac_toe.cpp
  )

add_library(${PROJECT_NAME} ${${PROJECT_NAME}_SRC_FILES})

add_executable(run_tests
  test/utest.cpp
)

target_link_libraries(run_tests
  ${PROJECT_NAME}
  Threads::Threads
  ${GTEST_LIBRARIES}
)

enable_testing()
add_test(NAME run_tests COMMAND run_tests)

add_executable(play_game src/play_game.cpp)
target_link_libraries(play_game ${PROJECT_NAME})