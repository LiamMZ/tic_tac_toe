# Tic Tac Toe
This is a tic-tac-toe command line game that you can play 1-Player againsta an AlphaBetaAI or 2-Player against another human.

## Running the Game
### Requirements
1. C++ 14
2. CMake VERSION 3.1.0+
3. GTest Package
4. Threads Package
### Compiling the Game
```
cd tic_tac_toe
mkdir build && cd build
cmake ..
make
```
### Playing a game of Tic Tac Toe
From the build directory:
```
./play_game
```
### Running test cases
From the build directory:
```
./run_tests
```

## Code Details
### Board
__1 2 3 <br>
1 X _ _ <br>
2 _ O _ <br>
3 _ O X <br>
Above is an example of the game board shown in command line. The default board is 3x3 with a starting index of 1.
## Players
For the 1-Player mode you can choose if you want to go first or second against the AlphaBetaAI.
### AlphaBetaAI
#### Hard (Alpha Beta) AlphaBetaAI
This AlphaBetaAI will choose the optimal move based on the [mini-max algorithm](https://en.wikipedia.org/wiki/Minimax) using the [alpha beta pruning](https://en.wikipedia.org/wiki/Alpha%E2%80%93beta_pruning) method.
