#include <command_line_interface.h>
#include <iostream>
#include <human.h>
#include <alpha_beta_ai.h>
#include <game_utils.h>

bool CLInterface::handlMenuInput(std::string input)
{
    // check if only one entry was provided
    if(input.length()!=1)
    {
        std::cout<<"Incorrect number of values entered. Please provide a valid menu option.\n";
        return false;
    }
    //check if input is a valid integer
    if(input[0]!='1' && input[0]!='2')
    {
        std::cout<<"Invalid value entered. Please provide a valid menu option.\n";
        return false;
    }
    return true;
}

template <class T1, class T2>
std::string CLInterface::play_game(T1 player1, T2 player2, TicTacToe& game)
{
    // turn limit in case no return happens
    int turn_limit = game.n_*game.n_;
    int turn = 0;
    // loop for allotted turns
    while(turn <= turn_limit)
    {
        // allow each player to move
        for(int i = 1; i<=2;i++)
        {   
            Move move;
            if(i==1)
            {
                move = player1.play(game);
            } 
            else
            {
                move = player2.play(game);
            } 
            // update state with move
            game.state = game.result(move, game.state);
            // check if game is over
            if( game.game_over(game.state))
            {
                // display final board
                game.display(game.state);
                // check if there is a winner or a draw
                if(game.state.utility==0)
                {
                    return "The game ends in a draw!\n";
                }
                std::string winner = game.state.to_move=='O'? "X":"O";
                return "Player-"+winner+" wins!\n";
            }
            turn++;
        }
    }
}

void CLInterface::run(int n)
{
    std::string choice;
    TicTacToe game(n);
    // loop until valid input is found
    while (true)
    {
        std::cout<<"Welcome to Tic-Tac-Toe! Please enter an option below:\n";
        std::cout<<"1 - Play solo against an AlphaBetaAI\n";
        std::cout<<"2 - Play a 2 player game against a friend\n";
        std::getline(std::cin,choice);
        bool success = handlMenuInput(choice);
        if(!success)
        {
            std::cout<<"Invalid input\n";
            continue;
        }
        break;
    }
    if (std::stoi(choice) == 1)
    {
        while(true){
            std::cout<<"Do you want to go first or second? (1/2): ";
            std::getline(std::cin,choice);
            bool success = handlMenuInput(choice);
            if(!success)
            {
                std::cout<<"Invalid input\n";
                continue;
            }
            break;
        }
        // 1-Player where human goes first selected
        if(std::stoi(choice)==1)
        {
            Human player1;
            AlphaBetaAI player2;
            std::cout<<play_game(player1,player2,game);
        }
        // 1-Player where human goes second selected
        else
        {
            AlphaBetaAI player1;
            Human player2;
            std::cout<<play_game(player1, player2, game);
        }
    }
    // 2-Player mode selected
    else
    {
        Human player1;
        Human player2;
        std::cout<<play_game(player1, player2, game);
    }
    
}