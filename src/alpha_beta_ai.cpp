#include <alpha_beta_ai.h>

Move AlphaBetaAI::play(TicTacToe& game)
{
    return alphabeta_search(game);
}

Move AlphaBetaAI::alphabeta_search(TicTacToe& game)
{
    //initialize alpha and beta and run minimax
    int alpha = INT8_MIN; // best/highest score found so far for Max
    int beta = INT8_MAX; // best/lowest score found so far for Min
    int value;
    return max_value(game, game.state, alpha, beta, value);
}

Move AlphaBetaAI::max_value(TicTacToe& game, State state, int alpha, int beta, int& value)
{
    // If terminal state return utility of state
    if(game.game_over(state))
    {
        value = game.utility(state, game.state.to_move);
        return Move(-1,-1);
    }
    //initialize value to worst case
    value = INT8_MIN;
    Move best_move, hold;
    // loop through available moves
    for(auto move : state.moves)
    {
        int resp;
        // get result from next level of game tree
        hold = min_value(game, game.result(move, state), alpha, beta, resp);
        // if the score returned is better than current value
        // update value and best_move
        if(resp > value)
        {
            value = resp;
            best_move = move;
        }
        // prune this branch of game tree if value is greater than beta
        if (value>=beta) return best_move;
        // update alpha
        alpha = std::max(alpha, value);
    }
    return best_move;
}

Move AlphaBetaAI::min_value(TicTacToe& game, State state, int alpha, int beta, int& value)
{
    // if state is a terminal state return utility
    if(game.game_over(state))
    {
        value = game.utility(state, game.state.to_move);
        return Move(-1,-1);
    }
    // initialize value to worst case
    value = INT8_MAX;
    Move best_move, hold;
    //itterate over available moves
    for( auto move : state.moves)
    {
        int resp;
        // get result from next level in game tree
        hold = max_value(game, game.result(move, state), alpha, beta, resp);
        // if response was better than current value
        // update current value and best move
        if(resp<value)
        {
            value = resp;
            best_move = move;
        }
        // if response was less than alpha prune branch
        if(resp<=alpha) return best_move;
        // update beta
        beta = std::min(beta,value);
    }
    return best_move;
}