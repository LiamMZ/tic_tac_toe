#include <human.h>
#include <iostream> 

Move Human::play(TicTacToe& game)
{
    return handle_input(game);
}

bool Human::validate_input(std::string row, std::string col, TicTacToe& game, Move& move)
{
    //check if row is a valid integer
    for(int i = 0; i<row.size(); i++)
    {
        if(!('0'<=row[i] && row[i]<='9'))
        {
            std::cout<<"Invalid row input entered. Please provide a valid row.\n";
            return false;
        }
    }
    // check if col is a valid integer
    for(int i = 0; i<col.size(); i++)
    {
        if(!('0'<=col[i] && col[i]<='9'))
        {
            std::cout<<"Invalid column input entered. Please provide a valid column.\n";
            return false;
        }
    }
    // check if move is in bounds
    if (std::stoi(row)<=0 || std::stoi(col)<=0 || std::stoi(row)>game.n_ || std::stoi(col)>game.n_)
    {
        std::cout<<"Input out of bounds.\n";
        return false;
    }
    move.row = std::stoi(row);
    move.col = std::stoi(col);
    // check if move is already taken
    if (game.state.moves.find(move) == game.state.moves.end())
    {
        std::cout<<"Move already taken.\n";
        return false;
    }
    return true;
}

Move Human::handle_input(TicTacToe& game)
{
    // hold current player
    std::string player(1, game.state.to_move);
    // loop until valid input is provided
    while(true)
    {
        // show current game board
        game.display(game.state);
        std::string row,col;
        // take user input for row and do initial validation
        std::cout<<"Player-"+player+" please enter the row for your move: ";
        std::getline(std::cin,row);        
        // take user input for col and do initial validation
        std::cout<<"\nPlayer-"+player+" please enter the column for your move: ";
        std::getline(std::cin,col);
        Move move;
        // Validate user input
        bool success = this->validate_input(row,col,game, move);
        if(!success)
        {
            std::cout<<"The coordinates you entered were invalid please enter valid coordinates.\n";
            continue;
        }
        return move;
    }
}