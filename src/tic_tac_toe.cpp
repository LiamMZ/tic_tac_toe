#include <algorithm>
#include <tic_tac_toe.h>
#include <iostream>

TicTacToe::TicTacToe(int n) : n_(n)
{
    //initialize board and available moves
    for(int i = 1; i<=n_; i++)
    {
        for(int j = 1; j<=n_; j++)
        {
            this->state.moves.insert(Move(i,j));
            this->state.board[Move(i,j)] = '_';
        }
    }
}

State TicTacToe::result(Move move, State state)
{
    // if the returned move is not available return
    if (state.moves.find(move) == state.moves.end())
    {
        return state;
    }
    // set utility of new state
    state.utility = this->compute_utility(move, state);
    // update board of new state
    state.board[move] = state.to_move;
    // remove move from available moves in new state
    state.moves.erase(move);
    // update player to move
    state.to_move = state.to_move == 'X' ? 'O' : 'X';
    return state;
}

int TicTacToe::compute_utility(Move move, State state)
{
    //hold row and col
    int row = move.row;
    int col = move.col;
    //hold player
    char player = state.to_move;
    // make a copy of board to compute hypothetical utility
    state.board[move] = player;
    int scores[4] = {0,0,0,0};
    for(int c = 1; c<=n_; c++)
    {
        //check row wise win
        scores[0] += state.board.at(Move(row, c)) == player;
        // check for col wise win
        scores[1]+= state.board.at(Move(c, col)) == player;
        // Check for Upper Left -> Lower Right diagonal win
        scores[2]+= state.board.at(Move(c, c)) == player;
        //check for Upper Right -> lower left Diagonal win
        scores[3]+= state.board.at(Move(n_-c+1, c)) == player;
    }
    
    for(int score : scores)
    {
        if(score == n_) return player == 'X' ? 1 : -1; 
    }
    return 0;
}

bool TicTacToe::game_over(State& state)
{
    return state.utility!=0 || state.moves.size() == 0;
}

int TicTacToe::utility(State& state, char player)
{
    return player=='X' ? state.utility : -state.utility;
}

void TicTacToe::display(State& state)
{
    std::unordered_map<Move, char, MyHashFunction> board = state.board;
    std::cout<<"Board:\n";
    // print column indicies
    std::cout<<"__";
    for(int col = 1; col<=n_; col++)
    {
        std::cout<<col<<" ";
    }
    std::cout<<"\n";
    for(int row = 1; row<=n_; row++)
    {   
        // print row indicies
        std::cout<<row<<" ";
        // print board values
        for(int col = 1; col<=n_; col++)
        {
            std::cout<<board.at(Move(row, col))<< " ";
        }
        std::cout<<"\n";
    }

}